//NAME: main.js
//DESCRIPTION: This file takes a csv file,date and time from html and prints hotel names which are opened on that day and time
//INPUT: gets input csv file,date and time from html file i.e main.html
//OUTPUT: Hotel names which are opened on that day and time 
function get_hotels() {
    const days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

    //processiong date and time input to day of the week and time----start
    var date = document.getElementById("date").value;
    var time = document.getElementById("time").value;
    var date_time = new Date(date + " " + time);
    var index_of_day = date_time.getDay() - 1;
    var day = days[index_of_day];
    var hour = date_time.getHours();
    var mins = date_time.getMinutes();
    var time_in_12 = get_time_in_12(hour, mins);
    var in_mins = hour * 60 + mins;
    //processiong date and time input to get day of the week and time----end

    //processing file which with id fileUpload----start
    var fileUpload = document.getElementById("fileUpload");
    //regular expression to check whether the file is csv or not
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+.csv$/;
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var rows = e.target.result.split("\n");//split csv file into lines

                document.write("Below hotels will be opened on <strong>"+day+" "+time_in_12+"</strong><br>"); 

                for (var i = 0; i < rows.length - 1; i++) {
                    rows[i] = rows[i].replace(/\"/g, "");//replacing " with nothing
                    var cells = rows[i].split(",");//split the rows into 2 cells  
                    var hotel = cells[0];//hotel name
                    var str = "";

                    //concating all cells except 1st cell to get time details
                    for (var j = 1; j < cells.length; j++) {
                        str += cells[j];
                    }

                    //splitting time details using the delimiter '/'
                    var open_times = str.split("/");//this array will have opened days and timings

                    //processing multiple timings of a hotel one by one---start
                    for (k = 0; k < open_times.length; k++) {
                        //processing open_times array to get opening and closing days of hotel----start
                        var low_day = -1;
                        var high_day = -1;
                        var reg = /(\w{3}-\w{3})/i;
                        if (reg.test(open_times[k])) {
                            var day_limit = open_times[k].match(reg);
                            var days_separated = day_limit[0].split('-');
                            low_day = days.indexOf(days_separated[0]);
                            high_day = days.indexOf(days_separated[1]);
                        }
                        //processing open_times array to get opened days of hotel----end

                        //processing  open_times array to get opened times of hotel----start
                        var time1_to = 0;
                        var time2_tot = 0;
                        var min1 = 0;
                        var min2 = 0;
                        var ind = 0;
                        var time_csv = open_times[k].match(/[0-9]+|am|pm|:/g);
                        for (var l = 0; l < time_csv.length; l++) {
                            if ((time_csv[l] == 'am' || time_csv[l] == 'pm') && l != time_csv.length - 1) {
                                ind = l + 1;
                            }
                        }
                        hr1 = time_csv[0];
                        if (time_csv[1] == ':') {
                            min1 = parseInt(time_csv[2], 10);
                        }
                        if (time_csv[2] == 'am' || time_csv[3] == 'am' || time_csv[1] == 'am') {
                            if (hr1 == 12) {
                                time1_tot = min1;
                            } else {
                                time1_tot = parseInt(hr1, 10) * 60 + min1;
                            }
                        } else {
                            time1_tot = (parseInt(hr1, 10) + 12) * 60 + min1;
                        }
                        hr2 = time_csv[ind];

                        if (time_csv[ind + 1] == ':') {
                            min2 = parseInt(time_csv[ind + 2], 10);
                        }
                        if (time_csv[time_csv.length - 1] == 'am') {

                            if (hr2 == 12) {
                                time2_tot = min2;
                            } else {
                                time2_tot = parseInt(hr2, 10) * 60 + min2;
                            }
                        } else {
                            if (hr2 == 12) {
                                time2_tot = (parseInt(hr2, 10)) * 60 + min2;
                            } else {
                                time2_tot = (parseInt(hr2, 10) + 12) * 60 + min2;
                            }
                        }
                        //processing  open_times array to get opening and closing times of hotel----end

                        //matching the day and checking for the time
                        if (open_times[k].includes(day[0])) {
                            if (check(time1_tot, time2_tot, in_mins)) {
                                document.write("<strong>" + hotel + "</strong><br>");
                                break;
                            }
                        } 
                        //checking whether the day comes in range and checking the time
                        else if (index_of_day >= low_day && index_of_day <= high_day) {
                            if (check(time1_tot, time2_tot, in_mins)) {
                                document.write("<strong>" + hotel + "</strong><br>");
                            }
                        }
                    }
                    //processing multiple timings of a hotel one by one---end
                }
            }
            reader.readAsText(fileUpload.files[0]);
        } 
        else {
            alert("This browser does not support HTML5.");
        }
    } 
    else {
        alert("Please upload a valid CSV file.");
    }
    //processing file which with id fileUpload----end
}

//fucntion to check whether the time in_mins comes in range between time1_tot and time2_tot
function check(time1_tot, time2_tot, in_mins) {
    while (true) {
        time1_tot = (time1_tot + 1) % (24*60);
        if (time1_tot == in_mins)
            return true;
        if (time1_tot == time2_tot) {
            return false;
        }
    }
}

//fuction to convert 24 hour format to 12 hour format
function get_time_in_12(hour, mins) {
    var time_in_12;
    if (hour == 12) {
        if(mins > 0) {
            time_in_12 = hour + ":" + mins + " " + "pm";
        }
        else {
            time_in_12 = hour + " " + "pm";   
        }
        
    }
    if (hour > 12) {
        if(mins > 0) {
            time_in_12 = hour - 12 + ":" + mins + " " + "pm";    
        }
        else {
            time_in_12 = hour - 12 + " " + "pm";
        }
        
    }
    if (hour == 0) {
        if(mins > 0){
           time_in_12 = "12:" + mins + " " + "am"; 
        }
        else {
            time_in_12 = "12:" + mins + " " + "am";
        }
    }
    if (hour > 0 && hour < 12) {
        if(mins > 0) {
            time_in_12 = hour + ":" + mins + " " + "am";
        }
        else {
            time_in_12 = hour + " " + "am";
        }
    }
    return time_in_12;
}